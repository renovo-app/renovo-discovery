FROM openjdk:11

COPY target/discovery-*.jar /discovery.jar

EXPOSE 8761

CMD ["java", "-jar", "/discovery.jar"]

